package com.hsd.rssreader.test;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.Channel;
import com.hsd.rssreader.provider.DataProvider;
import com.hsd.rssreader.utils.PrimitiveUtils;

public class DataProviderTestCase extends ProviderTestCase2<DataProvider> {

	public DataProviderTestCase(Class<DataProvider> providerClass, String providerAuthority) {
		super(providerClass, providerAuthority);
	}

	public DataProviderTestCase() {
		super(DataProvider.class, Consts.AUTHORITY);
	}

	// A URI that the provider does not offer, for testing error handling.
	private static final Uri INVALID_URI = Uri.withAppendedPath(Consts.CONTENT_CHANNEL_URI, "invalid");

	// Contains a reference to the mocked content resolver for the provider
	// under test.
	private MockContentResolver mMockResolver;

	// Contains an SQLite database, used as test data
	private SQLiteDatabase mDb;

	// Contains the test data, as an array of NoteInfo instances.
	private final Channel[] TEST_CHANNEL = {
			new Channel(null, "Americas", "Americas", "http://rss.cnn.com/rss/edition_americas.rss", null, null, null),
			new Channel(null, "Asia", "Asia", "http://rss.cnn.com/rss/edition_asia.rss", null, null, null),
			new Channel(null, "Europe", "Europe", "http://rss.cnn.com/rss/edition_europe.rss", null, null, null),
			new Channel(null, "Middle East", "Middle East", "http://rss.cnn.com/rss/edition_meast.rss", null, null,
					null), };

	/*
	 * Sets up the test environment before each test method. Creates a mock
	 * content resolver, gets the provider under test, and creates a new
	 * database for the provider.
	 */
	@Override
	protected void setUp() throws Exception {
		// Calls the base class implementation of this method.
		super.setUp();

		// Gets the resolver for this test.
		mMockResolver = getMockContentResolver();

		/*
		 * Gets a handle to the database underlying the provider. Gets the
		 * provider instance created in super.setUp(), gets the
		 * DatabaseOpenHelper for the provider, and gets a database object from
		 * the helper.
		 */
		// mDb = getProvider().getOpenHelperForTest().getWritableDatabase();
	}

	/*
	 * This method is called after each test method, to clean up the current
	 * fixture. Since this sample test case runs in an isolated context, no
	 * cleanup is necessary.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Sets up test data. The test data is in an SQL database. It is created in
	 * setUp() without any data, and populated in insertData if necessary.
	 */
	private void insertData() {
		// Sets up test data
		for (int index = 0; index < TEST_CHANNEL.length; index++) {
			ContentValues values = new ContentValues();
			values.put(Consts.DB_COLUMN_CHANNEL_TITLE, TEST_CHANNEL[index].getTitle());
			values.put(Consts.DB_COLUMN_CHANNEL_USER_TITLE, TEST_CHANNEL[index].getUserTitle());
			values.put(Consts.DB_COLUMN_CHANNEL_LINK, TEST_CHANNEL[index].getLink());
			values.put(Consts.DB_COLUMN_CHANNEL_LINK_MD5, PrimitiveUtils.md5(TEST_CHANNEL[index].getLink()));
			// Adds a record to the database.
			getProvider().insert(Consts.CONTENT_CHANNEL_URI, values);
		}
	}

	/*
	 * Tests the provider's publicly available URIs. If the URI is not one that
	 * the provider understands, the provider should throw an exception. It also
	 * tests the provider's getType() method for each URI, which should return
	 * the MIME type associated with the URI.
	 */
	public void testUriAndGetType() {
		// Tests the MIME type for the notes table URI.
		String mimeType = mMockResolver.getType(Consts.CONTENT_CHANNEL_URI);
		assertEquals(Consts.CONTENT_CHANNEL_URI, mimeType);

		// Tests the MIME type for the live folder URI.
		mimeType = mMockResolver.getType(Consts.CONTENT_FEEDITEM_URI);
		assertEquals(Consts.CONTENT_FEEDITEM_URI, mimeType);

		// Tests an invalid URI. This should throw an IllegalArgumentException.
		mimeType = mMockResolver.getType(INVALID_URI);
	}

}
